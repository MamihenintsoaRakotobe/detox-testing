/* eslint-disable no-undef */
describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  /*
  beforeEach(async () => {
    //await device.reloadReactNative();
  });
  */

  it('should have welcome screen', async () => {
    await expect(element(by.id('test'))).toBeVisible();
  });

  it('should type input', async () => {
    //await element(by.id('input')).typeText('Tiavina');
    await element(by.id('input')).replaceText('Tiavina');
    //await element(by.id('button-signin-confirm')).tap();
  });

  it('should tap button', async () => {
    await waitFor(element(by.id('button-signin-confirm')))
      .toBeVisible()
      .withTimeout(2000);
      await element(by.id('button-signin-confirm')).tap();
      //await element(by.id('button-signin-confirm')).tap();
  });

  /*
  it('should verify the text existence', async () => {
    await waitFor(element(by.id('loading')))
      .toBeVisible()
      .withTimeout(3000);
  });
  */
});
