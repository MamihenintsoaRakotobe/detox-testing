/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-undef */
import React, {FC} from 'react';
import {Button, Text, Icon, View} from 'native-base';
import {ActivityIndicator, Platform, StyleProp, ViewStyle} from 'react-native';
//import {RFValue} from 'react-native-responsive-fontsize';

interface CustomButtonProps {
  onPress(): void;
  loading?: boolean;
  title: string;
  info?: boolean;
  bordered?: boolean;
  disabled?: boolean;
  iconName?: string;
  iconType?: string;
  positionIcon?: string;
  transparent?: boolean;
  style?: StyleProp<ViewStyle>;
  testID?: string;
}

const CustomButton: FC<CustomButtonProps> = props => {
  const {
    title,
    onPress,
    loading,
    info,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    bordered,
    disabled,
    iconName,
    iconType,
    positionIcon,
    transparent,
    style,
    testID,
  } = props;

  return (
    <Button
      transparent={transparent}
      iconRight
      bordered
      info={Boolean(info)}
      disabled={disabled || loading}
      style={[
        {
          borderRadius: 10,
          marginVertical: 20,
          minHeight: Platform.OS === 'ios' ? 48 : 50,
        },
        style,
      ]}
      full
      onPress={onPress}>
      {loading ? (
        <ActivityIndicator color="#ecf0f1" />
      ) : (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          testID={testID ? testID : 'button-'}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-evenly',
          }}>
          {iconName && positionIcon === 'left' && (
            <Icon
              style={{
                fontSize: 25,
                color: disabled ? 'red' : info ? 'yellow' : 'green',
                marginRight: 10,
              }}
              name={iconName}
              type={iconType ? (iconType as any) : 'AntDesign'}
            />
          )}
          <Text
            // eslint-disable-next-line react-native/no-inline-styles

            style={{
              flexShrink: 1,
              fontSize: Platform.OS === 'ios' ? 16 : 18,
              fontWeight: 'bold',
              color: disabled ? 'red' : info ? 'yellow' : 'blue',
              textAlign: 'center',
              justifyContent: 'center',
              textAlignVertical: 'center',
            }}>
            {title.substring(0, 1).toUpperCase() +
              title.substring(1).toLocaleLowerCase()}
          </Text>
          {iconName && positionIcon !== 'left' && (
            <Icon
              style={{
                fontSize: 25,
                color: disabled ? 'red' : info ? 'yellow' : 'blue',
                marginLeft: 15,
              }}
              name={iconName}
              type={iconType ? (iconType as any) : 'AntDesign'}
            />
          )}
        </View>
      )}
    </Button>
  );
};

export default CustomButton;
